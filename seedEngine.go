package main

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"strings"
	"text/template"

	"github.com/Masterminds/sprig"
	"gopkg.in/yaml.v3"
)

type Kind struct {
	Kind string `yaml:"kind"`
}

type SeedEngine struct {
	EngineConfig  EngineConfig
	SeedVariables SeedVariables
	AwsConfig     AwsConfig
	HcloudConfig  HcloudConfig
}

func InitSeedEngine(publicSeedYamlfile string, sensitiveSeedYamlFile string) (*SeedEngine, error) {
	engine := &SeedEngine{}
	engine.HcloudConfig.Kind = "hcloudconfig"
	engine.AwsConfig.Kind = "awsconfig"
	engine.SeedVariables.Kind = "variables"
	engine.EngineConfig.Kind = "config"
	err := engine.parseVariablesFile(publicSeedYamlfile)
	if err != nil {
		return nil, err
	}
	err = engine.parseVariablesFile(sensitiveSeedYamlFile)
	if err != nil {
		return nil, err
	}

	return engine, nil
}

func (engine *SeedEngine) DumpToYaml(values dynMap) string {
	y, err := yaml.Marshal(&values)
	if err != nil {
		fmt.Printf("An error occured during marshaling: %s\n", err)
	}
	return string(y)
}

func (engine *SeedEngine) ProcessCloudInitTemplate() (string, error) {
	if engine.EngineConfig.CloudInitTemplate == "" {
		return "", errors.New("no cloud init template configured")
	}

	contentBytes, err := ioutil.ReadFile(engine.EngineConfig.CloudInitTemplate)
	if err != nil {
		return "", err
	}
	funcMap := template.FuncMap{
		"dumpToYaml": func(values dynMap) string { return engine.DumpToYaml(values) },
	}
	for k, v := range sprig.FuncMap() {
		funcMap[k] = v
	}
	tplt, err := template.New("").Funcs(funcMap).Parse(string(contentBytes))
	if err != nil {
		return "", err
	}
	var buffer bytes.Buffer
	tplt.Execute(&buffer, engine)
	return buffer.String(), nil

}

func (engine *SeedEngine) parseVariablesFile(fileName string) error {
	contentBytes, err := ioutil.ReadFile(fileName)
	if err != nil {
		return err
	}

	yamlDocsContent := strings.Split(string(contentBytes), "---")

	for _, v := range yamlDocsContent {
		kind := &Kind{}
		err := yaml.Unmarshal([]byte(v), &kind)
		if err != nil {
			return err
		}
		if kind.Kind == "config" {
			fmt.Println("config found")
			config := &EngineConfig{}
			err = yaml.Unmarshal([]byte(v), &config)
			if err != nil {
				return err
			}
			//merge config
			engine.EngineConfig = mergeEngineConfig(engine.EngineConfig, *config)

		} else if kind.Kind == "hcloudconfig" {
			fmt.Println("hcloudconfig found")
			config := &HcloudConfig{}
			err = yaml.Unmarshal([]byte(v), &config)
			if err != nil {
				return err
			}
			//merge config
			engine.HcloudConfig = mergeHcloudConfig(engine.HcloudConfig, *config)
		} else if kind.Kind == "awsconfig" {
			fmt.Println("awsconfig found")
			config := &AwsConfig{}
			err = yaml.Unmarshal([]byte(v), &config)
			if err != nil {
				return err
			}
			//merge config
			engine.AwsConfig = mergeAwsConfig(engine.AwsConfig, *config)
		} else if kind.Kind == "variables" {
			fmt.Println("variables found")
			vars := &SeedVariables{}
			e := yaml.Unmarshal([]byte(v), &vars)
			if err != nil {
				return e
			}
			//merge variables
			if engine.SeedVariables.Vars == nil {
				engine.SeedVariables.Vars = vars.Vars
			} else {
				engine.SeedVariables.Vars = mergeKeys(engine.SeedVariables.Vars, vars.Vars)
			}
		} else if kind.Kind == "" {
			fmt.Printf("ignoring empty kind in file: %s \n", fileName)
		} else {
			return errors.New(fmt.Sprintf("unknown yaml document kind: %s in file: %s", kind, fileName))
		}
	}

	return nil
}

//Merge dynamic yaml structue

func mergeKeys(left, right dynMap) dynMap {
	for key, rightVal := range right {
		if leftVal, present := left[key]; present {
			switch t := leftVal.(type) {
			case []interface{}:
				fmt.Println(t)
				left[key] = mergeMapArray(leftVal.([]interface{}), rightVal.([]interface{}))
			case dynMap:
				fmt.Println(t)
				left[key] = mergeKeys(leftVal.(dynMap), rightVal.(dynMap))
			}
		} else {
			left[key] = rightVal
		}
	}
	return left
}

func mergeMapArray(left, right []interface{}) []interface{} {
	foundKey := false
	for _, rightVal := range right {
		for nestedRightKey, nestedRightValue := range rightVal.(dynMap) {
			for _, leftValue := range left {
				if leftVal, present := leftValue.(dynMap)[nestedRightKey]; present {
					foundKey = true
					leftValue.(dynMap)[nestedRightKey] = mergeKeys(leftVal.(dynMap), nestedRightValue.(dynMap))
					fmt.Println(nestedRightKey)
				}
			}
			if !foundKey {
				left = append(left, rightVal)
			}
		}
	}
	return left
}

func mergeEngineConfig(left, right EngineConfig) EngineConfig {
	if right.SeedMachineName != "" {
		left.SeedMachineName = right.SeedMachineName
	}
	if right.CloudInitTemplate != "" {
		left.CloudInitTemplate = right.CloudInitTemplate
	}
	if right.Provider != "" {
		left.Provider = right.Provider
	}
	return left
}

func mergeAwsConfig(left, right AwsConfig) AwsConfig {
	if right.KeyName != "" {
		left.KeyName = right.KeyName
	}
	if right.Region != "" {
		left.Region = right.Region
	}
	if right.ImageId != "" {
		left.ImageId = right.ImageId
	}
	if right.InstanceType != "" {
		left.InstanceType = right.InstanceType
	}
	if right.RoleArn != "" {
		left.RoleArn = right.RoleArn
	}

	if right.SecurityGroupConfig.Name != "" {
		left.SecurityGroupConfig.Name = right.SecurityGroupConfig.Name
	}

	if right.SecurityGroupConfig.Description != "" {
		left.SecurityGroupConfig.Description = right.SecurityGroupConfig.Description
	}
	left.SecurityGroupConfig.InboundRules = append(left.SecurityGroupConfig.InboundRules, right.SecurityGroupConfig.InboundRules...)

	if right.IamProfile != "" {
		left.IamProfile = right.IamProfile
	}

	if right.Vpc.VpcCidr != "" {
		left.Vpc.VpcCidr = right.Vpc.VpcCidr
	}

	if right.Vpc.SubnetId != "" {
		left.Vpc.SubnetId = right.Vpc.SubnetId
	}

	if right.Vpc.VpcId != "" {
		left.Vpc.VpcId = right.Vpc.VpcId
	}

	if right.Vpc.Create {
		left.Vpc.Create = right.Vpc.Create
	}

	if right.Vpc.CreateInternetGateway {
		left.Vpc.CreateInternetGateway = right.Vpc.CreateInternetGateway
	}

	if right.Vpc.CreateNatGateway {
		left.Vpc.CreateNatGateway = right.Vpc.CreateNatGateway
	}

	if right.Vpc.PublicSubnet {
		left.Vpc.PublicSubnet = right.Vpc.PublicSubnet
	}

	if right.Vpc.PublicSubnetCidr != "" {
		left.Vpc.PublicSubnetCidr = right.Vpc.PublicSubnetCidr
	}

	if right.Vpc.PrivateSubnet {
		left.Vpc.PrivateSubnet = right.Vpc.PrivateSubnet
	}

	if right.Vpc.PublicSubnetCidr != "" {
		left.Vpc.PrivateSubnetCidr = right.Vpc.PrivateSubnetCidr
	}

	if right.Vpc.CreateSsmEndpoints {
		left.Vpc.CreateSsmEndpoints = right.Vpc.CreateSsmEndpoints
	}

	if right.Nlb.Create {
		left.Nlb.Create = right.Nlb.Create
	}

	if right.Nlb.Port != "" {
		left.Nlb.Port = right.Nlb.Port
	}

	if right.Tags != nil {
		if left.Tags == nil {
			left.Tags = right.Tags
		} else {
			for k, v := range right.Tags {
				left.Tags[k] = v
			}
		}
	}

	return left
}

func mergeHcloudConfig(left, right HcloudConfig) HcloudConfig {
	if right.ApiToken != "" {
		left.ApiToken = right.ApiToken
	}

	if right.SshKeys != nil {
		left.SshKeys = right.SshKeys
	}

	if right.ServerType != "" {
		left.ServerType = right.ServerType
	}

	if right.Location != "" {
		left.Location = right.Location
	}

	if right.Image != "" {
		left.Image = right.Image
	}
	if right.Labels != nil {
		if left.Labels == nil {
			left.Labels = right.Labels
		} else {
			for k, v := range right.Labels {
				left.Labels[k] = v
			}
		}
	}

	return left
}
