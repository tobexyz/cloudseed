package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type HcloudAdapter struct {
	ServerUrl   string
	ApiToken    string
	SeedRequest *SeedRequest
}

type SeedRequest struct {
	Name             string            `json:"name"`
	ServerType       string            `json:"server_type"`
	StartAfterCreate bool              `json:"start_after_create"`
	Image            string            `json:"image"`
	SshKeys          []string          `json:"ssh_keys"`
	UserData         string            `json:"user_data"`
	Labels           map[string]string `json:"labels"`
	Location         string            `json:"location"`
}

func InitHcloudAdapter(config EngineConfig, hcloudConfig HcloudConfig) (*HcloudAdapter, error) {
	adapter := &HcloudAdapter{
		ServerUrl: "https://api.hetzner.cloud/v1/servers",
		ApiToken:  hcloudConfig.ApiToken,
	}
	labels := make(map[string]string)
	if hcloudConfig.Labels != nil {
		labels = hcloudConfig.Labels
	}
	adapter.SeedRequest = &SeedRequest{
		Name:             config.SeedMachineName,
		ServerType:       hcloudConfig.ServerType,
		StartAfterCreate: true,
		SshKeys:          hcloudConfig.SshKeys,
		Image:            hcloudConfig.Image,
		Labels:           labels,
		Location:         hcloudConfig.Location,
	}

	return adapter, nil
}

func (h *HcloudAdapter) DeploySeed(cloudInit string) error {
	client := &http.Client{}
	h.SeedRequest.UserData = cloudInit
	seed, err := json.Marshal(h.SeedRequest)
	if err != nil {
		return err
	}
	if req, err := http.NewRequest("POST", h.ServerUrl, bytes.NewBuffer([]byte(seed))); err != nil {
		return err
	} else {
		req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", h.ApiToken))
		if resp, err := client.Do(req); err != nil {
			return err
		} else {
			defer resp.Body.Close()
			fmt.Println(resp.StatusCode)
			if body, err := ioutil.ReadAll(resp.Body); err != nil {
				return err
			} else {
				fmt.Printf("%s\n", body)
				return nil
			}
		}
	}
}
