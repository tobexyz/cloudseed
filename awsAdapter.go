package main

import (
	"encoding/base64"
	"errors"
	"fmt"
	"strconv"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/aws/aws-sdk-go/service/elbv2"
)

type AwsAdapter struct {
	awsConfig   *AwsConfig
	name        string
	Credentials *credentials.Credentials
}

func InitAwsAdapter(config *EngineConfig, awsConfig *AwsConfig) (*AwsAdapter, error) {
	adapter := &AwsAdapter{
		awsConfig: awsConfig,
		name:      config.SeedMachineName,
	}

	credentials := credentials.NewChainCredentials(
		[]credentials.Provider{
			&credentials.EnvProvider{},
			&credentials.SharedCredentialsProvider{},
		})
	adapter.Credentials = credentials
	return adapter, nil
}

func (a *AwsAdapter) DeploySeed(cloudInit string) error {
	sess, err := session.NewSession(&aws.Config{
		Region:      aws.String(a.awsConfig.Region),
		Credentials: a.Credentials,
	})
	if err != nil {
		return err
	}
	if a.awsConfig.RoleArn != "" {
		creds := stscreds.NewCredentials(sess, a.awsConfig.RoleArn)
		sess, err = session.NewSession(&aws.Config{
			Region:      aws.String(a.awsConfig.Region),
			Credentials: creds,
		})
		if err != nil {
			return err
		}
	}
	ec2Session := ec2.New(sess)

	var subnetId *string
	var vpcId *string
	var publicSubnetId *string
	var privateSubnetId *string

	if a.awsConfig.Vpc.Create {
		//create vpc
		vpcId, err = a.setupSeedVpc(ec2Session)
		if err != nil {
			return err
		}
		if a.awsConfig.Vpc.PublicSubnet {
			publicSubnetId, err = a.createPublicSubnet(ec2Session, vpcId)
			if err != nil {
				return err
			}
			subnetId = publicSubnetId
		}
		if a.awsConfig.Vpc.PrivateSubnet {
			privateSubnetId, err = a.createSubnet(ec2Session, vpcId, a.awsConfig.Vpc.PrivateSubnetCidr, fmt.Sprintf("%s-private-subnet", a.name), false)
			if err != nil {
				return err
			}
			subnetId = privateSubnetId
		}
		if a.awsConfig.Vpc.CreateNatGateway && a.awsConfig.Vpc.PrivateSubnet && a.awsConfig.Vpc.PublicSubnet {
			natGwId, err := a.createNatGateway(ec2Session, vpcId, publicSubnetId)
			if err != nil {
				return err
			}
			ec2Session.WaitUntilNatGatewayAvailable(&ec2.DescribeNatGatewaysInput{
				Filter: []*ec2.Filter{
					{
						Name: aws.String("vpc-id"),
						Values: []*string{
							vpcId,
						},
					},
				},
			})
			err = a.createRoutingTable(ec2Session, vpcId, privateSubnetId, natGwId, fmt.Sprintf("%s-nat-rt", a.name), false)
			if err != nil {
				return err
			}
		}
	} else {
		vpcId, err = a.findAnExistingVpc(ec2Session)
		if err != nil {
			return err
		}
		subnetId, err = a.findAnExistingSubnet(ec2Session, vpcId)
		if err != nil {
			return err
		}
	}

	var securityGroupId *string

	if a.awsConfig.SecurityGroupConfig.Name != "" {
		securityGroupId, err = a.createSecurityGroup(ec2Session, vpcId)
		if err != nil {
			return err
		}
	}
	securityGroupIds := []*string{securityGroupId}
	ec2InstanceId, err := a.createEc2Instance(ec2Session, securityGroupIds, subnetId, cloudInit)
	if err != nil {
		return err
	}
	err = a.associateIamInstanceProfile(ec2Session, ec2InstanceId)
	if err != nil {
		return err
	}

	if a.awsConfig.Nlb.Create && a.awsConfig.Vpc.PublicSubnet {
		instanceIds := []*string{ec2InstanceId}
		ec2instances := &ec2.DescribeInstancesInput{
			InstanceIds: instanceIds,
		}
		ec2Session.WaitUntilInstanceRunning(ec2instances)
		elbv2Session := elbv2.New(sess)
		err = a.createNetworkLoadbalancer(elbv2Session, vpcId, publicSubnetId, ec2InstanceId)
		if err != nil {
			return err
		}
	}
	return nil
}

func (a *AwsAdapter) createSecurityGroup(ec2Session *ec2.EC2, vpcId *string) (*string, error) {
	// Create the security group with the VPC, name and description.
	securityGroup, err := ec2Session.CreateSecurityGroup(&ec2.CreateSecurityGroupInput{
		GroupName:   aws.String(a.awsConfig.SecurityGroupConfig.Name),
		Description: aws.String(a.awsConfig.SecurityGroupConfig.Description),
		VpcId:       vpcId,
	})

	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			case "InvalidVpcID.NotFound":
				return nil, fmt.Errorf("unable to find vpc with id %q", *vpcId)
			case "InvalidGroup.Duplicate":
				return nil, fmt.Errorf("security group %q already exists", a.awsConfig.SecurityGroupConfig.Name)
			}
		}
		return nil, fmt.Errorf("unable to create security group %q, %v", a.awsConfig.SecurityGroupConfig.Name, err)
	}

	fmt.Printf("Created security group %s with VPC %s.\n", aws.StringValue(securityGroup.GroupId), *vpcId)
	err = a.setTags(ec2Session, securityGroup.GroupId, fmt.Sprintf("%s-sg", a.name), &a.awsConfig.Tags)

	if err != nil {
		return nil, err
	}
	fmt.Println("Successfully tagged security group")

	// Add permissions to the security group
	for _, rule := range a.awsConfig.SecurityGroupConfig.InboundRules {
		sgPort, err := strconv.ParseInt(rule.Port, 10, 64)
		if err != nil {
			return nil, fmt.Errorf("unable convert SeedSshSecurityGroupPort %q, %v", rule.Port, err)
		}
		_, err = ec2Session.AuthorizeSecurityGroupIngress(&ec2.AuthorizeSecurityGroupIngressInput{
			GroupId: securityGroup.GroupId,
			IpPermissions: []*ec2.IpPermission{
				(&ec2.IpPermission{}).
					SetIpProtocol("tcp").
					SetFromPort(sgPort).
					SetToPort(sgPort).
					SetIpRanges([]*ec2.IpRange{
						(&ec2.IpRange{}).
							SetCidrIp(rule.IpRange),
					}),
			},
		})
		if err != nil {
			return nil, fmt.Errorf("unable to set security group inbound rule %q port %q, %v", rule.IpRange, rule.Port, err)
		}
	}

	fmt.Println("Successfully set security group inbound rules")
	return securityGroup.GroupId, nil
}

func (a *AwsAdapter) setupSeedVpc(ec2Session *ec2.EC2) (*string, error) {
	vpc, err := ec2Session.CreateVpc(&ec2.CreateVpcInput{
		CidrBlock: aws.String(a.awsConfig.Vpc.VpcCidr),
	})
	if err != nil {
		return nil, err
	}
	fmt.Println("Successfully created vpc")
	vpcId := aws.String(*vpc.Vpc.VpcId)
	input := &ec2.ModifyVpcAttributeInput{
		EnableDnsSupport: &ec2.AttributeBooleanValue{
			Value: aws.Bool(true),
		},
		VpcId: vpcId,
	}

	_, err = ec2Session.ModifyVpcAttribute(input)
	if err != nil {
		return nil, err
	}
	fmt.Println("Successfully enableDnsSupport")

	input = &ec2.ModifyVpcAttributeInput{
		EnableDnsHostnames: &ec2.AttributeBooleanValue{
			Value: aws.Bool(true),
		},
		VpcId: vpcId,
	}

	_, err = ec2Session.ModifyVpcAttribute(input)
	if err != nil {
		return nil, err
	}
	fmt.Println("Successfully enableDnsHostnames ")
	err = a.setTags(ec2Session, vpcId, fmt.Sprintf("%s-vpc", a.name), &a.awsConfig.Tags)
	if err != nil {
		return nil, err
	}
	fmt.Println("Successfully tagged vpc")

	if a.awsConfig.Vpc.CreateSsmEndpoints {
		err = a.createVpcSsmEndpoints(ec2Session, vpcId)
		if err != nil {
			return nil, err
		}
	}
	return vpcId, nil
}
func (a *AwsAdapter) createPublicSubnet(ec2Session *ec2.EC2, vpcId *string) (*string, error) {
	//create public subnet
	subnetId, err := a.createSubnet(ec2Session, vpcId, a.awsConfig.Vpc.PublicSubnetCidr, fmt.Sprintf("%s-public-subnet", a.name), true)
	if err != nil {
		return nil, err
	}

	if a.awsConfig.Vpc.CreateInternetGateway {
		igwId, err := a.createInternetGateway(ec2Session, vpcId)
		if err != nil {
			return nil, err
		}
		err = a.createRoutingTable(ec2Session, vpcId, subnetId, igwId, fmt.Sprintf("%s-igw-rt", a.name), true)
		if err != nil {
			return nil, err
		}
	}
	return subnetId, nil
}
func (a *AwsAdapter) createSubnet(ec2Session *ec2.EC2, vpcId *string, cidr string, name string, public bool) (*string, error) {
	//create subnet in vpc
	subnet, err := ec2Session.CreateSubnet(&ec2.CreateSubnetInput{
		CidrBlock: aws.String(cidr),
		VpcId:     vpcId,
	})
	if err != nil {
		return nil, fmt.Errorf("unable to create subnet, %v", err)
	}
	fmt.Println("Successfully created subnet")
	subnetId := aws.String(*subnet.Subnet.SubnetId)
	err = a.setTags(ec2Session, subnetId, name, &a.awsConfig.Tags)
	if err != nil {
		return nil, err
	}
	fmt.Println("Successfully tagged subnet")
	if public {
		fmt.Println("modify subnet map public ip on launch")
		_, err = ec2Session.ModifySubnetAttribute(&ec2.ModifySubnetAttributeInput{
			MapPublicIpOnLaunch: &ec2.AttributeBooleanValue{
				Value: aws.Bool(true),
			},
			SubnetId: subnetId,
		})
		if err != nil {
			return nil, err
		}
		fmt.Println("Successfully modified subnet to public subnet")
	}

	return subnetId, nil
}

func (a *AwsAdapter) createNatGateway(ec2Session *ec2.EC2, vpcId *string, publicSubnetId *string) (*string, error) {

	eipalloc, err := ec2Session.AllocateAddress(
		&ec2.AllocateAddressInput{
			Domain: aws.String("vpc"),
		},
	)
	if err != nil {
		return nil, err
	}
	fmt.Println("Successfully allocated elastic ip")

	natgw, err := ec2Session.CreateNatGateway(
		&ec2.CreateNatGatewayInput{
			AllocationId: eipalloc.AllocationId,
			SubnetId:     publicSubnetId,
		},
	)
	if err != nil {
		return nil, err
	}
	err = a.setTags(ec2Session, natgw.NatGateway.NatGatewayId, fmt.Sprintf("%s-natgw", a.name), &a.awsConfig.Tags)
	if err != nil {
		return nil, err
	}
	fmt.Println("Successfully creates nat gateway")

	return natgw.NatGateway.NatGatewayId, nil
}
func (a *AwsAdapter) createVpcSsmEndpoints(ec2Session *ec2.EC2, vpcId *string) error {
	//ssm endpoints
	ei := &ec2.CreateVpcEndpointInput{
		VpcId:           vpcId,
		ServiceName:     aws.String(fmt.Sprintf("com.amazonaws.%s.ssm", a.awsConfig.Region)),
		VpcEndpointType: aws.String("Interface"),
	}

	_, err := ec2Session.CreateVpcEndpoint(ei)
	if err != nil {
		return err
	}

	ei = &ec2.CreateVpcEndpointInput{
		VpcId:           vpcId,
		ServiceName:     aws.String(fmt.Sprintf("com.amazonaws.%s.ssmmessages", a.awsConfig.Region)),
		VpcEndpointType: aws.String("Interface"),
	}

	_, err = ec2Session.CreateVpcEndpoint(ei)
	if err != nil {
		return err
	}

	ei = &ec2.CreateVpcEndpointInput{
		VpcId:           vpcId,
		ServiceName:     aws.String(fmt.Sprintf("com.amazonaws.%s.ec2messages", a.awsConfig.Region)),
		VpcEndpointType: aws.String("Interface"),
	}

	_, err = ec2Session.CreateVpcEndpoint(ei)
	if err != nil {
		return err
	}
	fmt.Println("Successfully created ssm endpoints on vpc")
	return nil
}
func (a *AwsAdapter) createNetworkLoadbalancer(elbv2Session *elbv2.ELBV2, vpcId *string, subnetId *string, instanceId *string) error {

	lbinput := &elbv2.CreateLoadBalancerInput{
		Name: aws.String("seed-nlb"),
		Type: aws.String(elbv2.LoadBalancerTypeEnumNetwork),
		Subnets: []*string{
			aws.String(*subnetId),
		},
	}
	lb, err := elbv2Session.CreateLoadBalancer(lbinput)
	if err != nil {
		return err
	}
	a.setElbTags(elbv2Session, lb.LoadBalancers[0].LoadBalancerArn, a.name, &a.awsConfig.Tags)
	fmt.Println("Created load balancer: ", *lb.LoadBalancers[0].DNSName)
	port, err := strconv.ParseInt(a.awsConfig.Nlb.Port, 10, 64)
	if err != nil {
		return err
	}
	tginput := &elbv2.CreateTargetGroupInput{
		Name:     aws.String("seed-ssh-target"),
		Port:     aws.Int64(port),
		Protocol: aws.String("TCP"),
		VpcId:    aws.String(*vpcId),
	}

	tg, err := elbv2Session.CreateTargetGroup(tginput)
	if err != nil {
		return err
	}
	a.setElbTags(elbv2Session, tg.TargetGroups[0].TargetGroupArn, a.name, &a.awsConfig.Tags)
	fmt.Println("Created target group: ", *tg.TargetGroups[0].TargetGroupName)

	regtginput := &elbv2.RegisterTargetsInput{
		TargetGroupArn: tg.TargetGroups[0].TargetGroupArn,
		Targets: []*elbv2.TargetDescription{
			{
				Id: instanceId,
			},
		},
	}

	_, err = elbv2Session.RegisterTargets(regtginput)
	if err != nil {
		return err
	}
	fmt.Println("Registered instance to target group")

	input := &elbv2.CreateListenerInput{
		DefaultActions: []*elbv2.Action{
			{
				TargetGroupArn: tg.TargetGroups[0].TargetGroupArn,
				Type:           aws.String("forward"),
			},
		},
		LoadBalancerArn: lb.LoadBalancers[0].LoadBalancerArn,
		Port:            aws.Int64(port),
		Protocol:        aws.String("TCP"),
	}

	_, err = elbv2Session.CreateListener(input)
	if err != nil {
		return err
	}
	fmt.Println("Registered listener for target group to load balancer")
	return nil
}

func (a *AwsAdapter) createRoutingTable(ec2Session *ec2.EC2, vpcId *string, subnetId *string, gwId *string, name string, igw bool) error {
	//create route table
	rtt, err := ec2Session.CreateRouteTable(&ec2.CreateRouteTableInput{
		VpcId: vpcId,
	})
	if err != nil {
		return err
	}
	fmt.Println("Successfully created routing table")
	rttId := rtt.RouteTable.RouteTableId
	err = a.setTags(ec2Session, rttId, name, &a.awsConfig.Tags)
	if err != nil {
		return err
	}

	fmt.Println("Successfully tagged routing table")
	if igw {
		_, err = ec2Session.CreateRoute(&ec2.CreateRouteInput{
			DestinationCidrBlock: aws.String("0.0.0.0/0"),
			GatewayId:            gwId,
			RouteTableId:         rttId,
		})
	} else {
		_, err = ec2Session.CreateRoute(&ec2.CreateRouteInput{
			DestinationCidrBlock: aws.String("0.0.0.0/0"),
			NatGatewayId:         gwId,
			RouteTableId:         rttId,
		})
	}

	if err != nil {
		return err
	}
	fmt.Println("Successfully created route")
	_, err = ec2Session.AssociateRouteTable(&ec2.AssociateRouteTableInput{
		RouteTableId: rttId,
		SubnetId:     subnetId,
	})
	if err != nil {
		return err
	}
	fmt.Println("Successfully associate routing table to subnet")
	return nil
}
func (a *AwsAdapter) createInternetGateway(ec2Session *ec2.EC2, vpcId *string) (*string, error) {
	//create internet gateway
	ig, err := ec2Session.CreateInternetGateway(&ec2.CreateInternetGatewayInput{})
	if err != nil {
		return nil, fmt.Errorf("unable to create internet gateway, %v", err)
	}
	igwId := aws.String(*ig.InternetGateway.InternetGatewayId)
	fmt.Println("Successfully created internet gateway")
	err = a.setTags(ec2Session, igwId, fmt.Sprintf("%s-igw", a.name), &a.awsConfig.Tags)
	if err != nil {
		return nil, err
	}
	fmt.Println("Successfully tagged intenet gateway")
	//attach internet gateway to vpc
	_, err = ec2Session.AttachInternetGateway(&ec2.AttachInternetGatewayInput{
		InternetGatewayId: igwId,
		VpcId:             vpcId,
	})
	if err != nil {
		return nil, err
	}
	fmt.Println("Successfully attached internet gateway to vpc")
	return igwId, nil
}
func (a *AwsAdapter) createEc2Instance(ec2Session *ec2.EC2, securityGroupIds []*string, subnetId *string, cloudInit string) (*string, error) {
	runResult, err := ec2Session.RunInstances(&ec2.RunInstancesInput{
		ImageId:          aws.String(a.awsConfig.ImageId),
		InstanceType:     aws.String(a.awsConfig.InstanceType),
		MinCount:         aws.Int64(1),
		MaxCount:         aws.Int64(1),
		KeyName:          aws.String(a.awsConfig.KeyName),
		UserData:         aws.String(base64.StdEncoding.EncodeToString([]byte(cloudInit))),
		SecurityGroupIds: securityGroupIds,
		SubnetId:         subnetId,
	})
	if err != nil {
		return nil, err
	}
	fmt.Println("Created instance", *runResult.Instances[0].InstanceId)
	err = a.setTags(ec2Session, runResult.Instances[0].InstanceId, a.name, &a.awsConfig.Tags)

	if err != nil {
		return nil, err
	}
	fmt.Println("Successfully tagged instance")
	return runResult.Instances[0].InstanceId, nil
}

func (a *AwsAdapter) associateIamInstanceProfile(ec2Session *ec2.EC2, ec2InstanceId *string) error {
	if a.awsConfig.IamProfile != "" {
		instanceIds := []*string{ec2InstanceId}
		ec2instances := &ec2.DescribeInstancesInput{
			InstanceIds: instanceIds,
		}
		ec2Session.WaitUntilInstanceRunning(ec2instances)
		input := &ec2.AssociateIamInstanceProfileInput{
			IamInstanceProfile: &ec2.IamInstanceProfileSpecification{
				Name: aws.String(a.awsConfig.IamProfile),
			},
			InstanceId: aws.String(*ec2InstanceId),
		}

		_, err := ec2Session.AssociateIamInstanceProfile(input)

		if err != nil {
			return err
		}
		fmt.Println(fmt.Sprintf("Successfully associated instance profile: '%s'", a.awsConfig.IamProfile))
	}
	return nil
}

func (a *AwsAdapter) findAnExistingVpc(ec2Session *ec2.EC2) (*string, error) {
	var vpcFilter *ec2.DescribeVpcsInput
	vpcFilter = nil
	if a.awsConfig.Vpc.VpcId != "" {
		vpcFilter = &ec2.DescribeVpcsInput{
			Filters: []*ec2.Filter{
				{
					Name:   aws.String("vpc-id"),
					Values: []*string{aws.String(a.awsConfig.Vpc.VpcId)},
				},
			},
		}
	}
	// Get a list of VPCs so we can associate the group with the first VPC.
	vpcs, err := ec2Session.DescribeVpcs(vpcFilter)
	if err != nil {
		return nil, fmt.Errorf("unable to describe vpcs, %v", err)
	}
	if len(vpcs.Vpcs) == 0 {
		return nil, errors.New("no vpcs found to use for ec2 instance")
	}
	fmt.Println(fmt.Sprintf("found vpc for id %s", a.awsConfig.Vpc.VpcId))
	return vpcs.Vpcs[0].VpcId, nil
}

func (a *AwsAdapter) findAnExistingSubnet(ec2Session *ec2.EC2, vpcId *string) (*string, error) {
	subnetFilter := []*ec2.Filter{
		{
			Name:   aws.String("vpc-id"),
			Values: []*string{vpcId},
		}}
	if a.awsConfig.Vpc.SubnetId != "" {
		filter := &ec2.Filter{
			Name:   aws.String("subnet-id"),
			Values: []*string{&a.awsConfig.Vpc.SubnetId},
		}
		subnetFilter = append(subnetFilter, filter)
	}
	subnets, err := ec2Session.DescribeSubnets(&ec2.DescribeSubnetsInput{Filters: subnetFilter})
	if err != nil {
		return nil, err
	}
	if len(subnets.Subnets) == 0 {
		return nil, errors.New("no subnets found to associate with instance")
	}
	fmt.Println(fmt.Sprintf("found subnet for id %s", a.awsConfig.Vpc.SubnetId))
	return subnets.Subnets[0].SubnetId, nil
}

func (a *AwsAdapter) setTags(ec2Session *ec2.EC2, resourceId *string, name string, additionalTags *map[string]string) error {

	var tags []*ec2.Tag
	tags = append(tags, &ec2.Tag{
		Key:   aws.String("Name"),
		Value: aws.String(name),
	})
	for k, v := range *additionalTags {
		tags = append(tags, &ec2.Tag{
			Key:   aws.String(k),
			Value: aws.String(v),
		})
	}
	_, err := ec2Session.CreateTags(&ec2.CreateTagsInput{
		Resources: []*string{resourceId},
		Tags:      tags,
	})
	return err
}

func (a *AwsAdapter) setElbTags(elbv2Session *elbv2.ELBV2, resourceId *string, name string, additionalTags *map[string]string) error {

	var tags []*elbv2.Tag
	tags = append(tags, &elbv2.Tag{
		Key:   aws.String("Name"),
		Value: aws.String(name),
	})
	for k, v := range *additionalTags {
		tags = append(tags, &elbv2.Tag{
			Key:   aws.String(k),
			Value: aws.String(v),
		})
	}
	_, err := elbv2Session.AddTags(&elbv2.AddTagsInput{
		ResourceArns: []*string{resourceId},
		Tags:         tags,
	})
	return err
}
