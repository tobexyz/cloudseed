
.PHONY : all golang_build golang_test golang_arm_build golang_win_build golang_mac_build

all: golang_build golang_test golang_arm_build golang_win_build golang_mac_build

test: golang_test

build: golang_build golang_arm_build golang_win_build golang_mac_build


golang_build:
	@go build -race -ldflags "-extldflags '-static'" -o cloudseed

golang_test:
	@go fmt $(go list ./... | grep -v /vendor/)
	@go vet $(go list ./... | grep -v /vendor/)
	@go test -race $(go list ./... | grep -v /vendor/)


golang_arm_build:
	@GOARCH=arm GOARM=7 go build -ldflags "-extldflags '-static'"  -o cloudseed_arm
golang_win_build:
	@GOOS=windows GOARCH=amd64 go build -ldflags "-extldflags '-static'"
golang_mac_build:
	@GOOS=darwin GOARCH=amd64 go build -ldflags "-extldflags '-static'" -o cloudseed_darwin


