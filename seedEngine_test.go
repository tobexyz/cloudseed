package main

import (
	"fmt"
	"github.com/stretchr/testify/suite"
	"gopkg.in/yaml.v3"
	"testing"
)

type SeedEngineTestSuite struct {
	suite.Suite
}

func TestSeedEngineTestSuite(t *testing.T) {
	suite.Run(t, new(SeedEngineTestSuite))
}

func (suite *SeedEngineTestSuite) TestInitSeedEngineHcloud() {
	engine, err := InitSeedEngine("./examples/seedfiles/seed_example.yaml", "./examples/credential_example.yaml")
	suite.NoError(err)
	if suite.NotNil(engine) {
		engineConfig := engine.EngineConfig
		seedVariables := engine.SeedVariables
		hcloudConfig := engine.HcloudConfig
		awsConfig := engine.AwsConfig

		suite.Equal([]string{"foo", "bar", "fee"}, hcloudConfig.SshKeys)
		suite.Equal("./examples/templates/cloudinit-example.yaml.tmpl", engineConfig.CloudInitTemplate)
		suite.Equal("sadflkasjfda", hcloudConfig.ApiToken)
		suite.Equal("blue", seedVariables.Vars["green_bar"])
		d, err := yaml.Marshal(seedVariables.Vars)
		if err != nil {
			suite.Error(err)
		}
		fmt.Printf("---SeedVariables.Vars dump:\n%s\n\n", string(d))

		d, err = yaml.Marshal(engineConfig)
		if err != nil {
			suite.Error(err)
		}
		fmt.Printf("--- EngineConfig dump:\n%s\n\n", string(d))

		d, err = yaml.Marshal(awsConfig)
		if err != nil {
			suite.Error(err)
		}
		fmt.Printf("--- AwsConfig dump:\n%s\n\n", string(d))

		d, err = yaml.Marshal(hcloudConfig)
		if err != nil {
			suite.Error(err)
		}
		fmt.Printf("--- HcloudConfig dump:\n%s\n\n", string(d))

		cloudinit, err := engine.ProcessCloudInitTemplate()
		if err != nil {
			suite.Error(err)
		}
		fmt.Printf("--- CloudInit dump:\n%s\n\n", string(cloudinit))
	}
}

func (suite *SeedEngineTestSuite) TestInitSeedEngineAws() {
	engine, err := InitSeedEngine("./examples/seedfiles/seed_example_aws.yaml", "./examples/credential_example.yaml")
	suite.NoError(err)
	if suite.NotNil(engine) {
		engineConfig := engine.EngineConfig
		seedVariables := engine.SeedVariables
		awsConfig := engine.AwsConfig
		hcloudConfig := engine.HcloudConfig

		suite.Equal([]string{"foo", "bar", "fee"}, hcloudConfig.SshKeys)
		suite.Equal("./examples/templates/cloudinit-example.yaml.tmpl", engineConfig.CloudInitTemplate)
		suite.Equal("ami-0b418580298265d5c", awsConfig.ImageId)
		suite.Equal("blue", seedVariables.Vars["green_bar"])
		d, err := yaml.Marshal(seedVariables.Vars)
		if err != nil {
			suite.Error(err)
		}
		fmt.Printf("---SeedVariables.Vars dump:\n%s\n\n", string(d))

		d, err = yaml.Marshal(engineConfig)
		if err != nil {
			suite.Error(err)
		}
		fmt.Printf("--- EngineConfig dump:\n%s\n\n", string(d))

		d, err = yaml.Marshal(awsConfig)
		if err != nil {
			suite.Error(err)
		}
		fmt.Printf("--- AwsConfig dump:\n%s\n\n", string(d))

		d, err = yaml.Marshal(hcloudConfig)
		if err != nil {
			suite.Error(err)
		}
		fmt.Printf("--- HcloudConfig dump:\n%s\n\n", string(d))

		cloudinit, err := engine.ProcessCloudInitTemplate()
		if err != nil {
			suite.Error(err)
		}
		fmt.Printf("--- CloudInit dump:\n%s\n\n", string(cloudinit))
	}
}
