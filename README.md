# cloudseed - start vms easily with minimum tool dependencies
## About
Spawn seedvms at a cloudprovider, based on a templated cloudinit file.
The seed vm itself can spawn other vms for example

currently following cloudproviders are supported:

- hcloud (Hetzner)
- aws

### Author
This tool was written by tobexyz tobexyz{at}schoenesnetz.de
### License
The tool is published under GPLv3


## Build
install golang

make
## Usage
Create a cloudinit-Template using the Golang template syntax.
Create two files containing sensive and public variables, used in the template.
The files must contain the following yaml structure.
During processing these files are merged and passed to the template.
Setup the provider specific configuration options. 

## Yaml structure

### Template engine config

```
kind: config
cloudinittemplate: pathToYourCloudInitTemplateFile
seedmachinename: theNameOfTheSeedMachine
provider: < hcloud or aws >
```

### Seedvariables file

```
kind: variables
vars:
 <insert your variables>
```  

### Hcloud provider config

```
kind: hcloudconfig
apitoken: yourApiToken
server-type: <ServerType for example: cx11>
image: <os image for example: ubuntu-18.04>
location: <location for example: nbg1>
ssh-keys: ["sshKey1", "sshKey2", "sshKey3", ...]
```
### Aws provider config

```
kind: awsconfig
region: eu-central-1
image-id: ami-0b418580298265d5c
instance-type: t2.micro
iam-profile: "ProfileName" #optional set an given iam profile on ec2 instance 
security-group: 
  name: "seed-sg"
  description: "SSH access to seed machine"
  inboundrules:
  - iprange: "192.168.0.0/16"
    port: "22"
  - iprange: "18.184.138.224/27" #cloud9 eu-central-1
    port: "22"
  - iprange: "18.184.203.128/27" #cloud9 eu-central-1
    port: "22"  
vpc: 
  create: true #if true create vpc otherwise use given ids or choose vpc and subnet by best effort 
  vpc-id: "asdfasdfsaf" #optional if set use this vpc instead of creating one
  subnet-id: "fdafasfd" #optional must be set together with vpc-id. if set use this subnet for placing ec2 instance  
  vpc-cidr: "192.168.0.0/16" #cidr of vpc which will be created
  subnet-cidr: "192.168.0.0/16" #cidr of subnet which will be created
  public-subnet: true  # make subnet accessible form the internet
  create-ssm-endpoints: false #if set ssm endpoints will be attached to the vpc and the instance profile will be set to ec2
  create-igw: true #create internet gateway in vpc 
```

## Template processing

The template processing is done by go template (https://golang.org/pkg/text/template/).
All config values are accessible in the template through the ```engine``` variable as shown below.
Attributes of yaml files must be accessed by using the spelling in the corresponding go type shown below.   

```
access the seed variable structure {.SeedVariables}
Assuming a variable foo is configured in your seed variables yaml file, you are able to access it like this: {{.SeedVariables.Vars.foo}}

access the engine config structure {{.EngineConfig}}
For example accessing the cloudinit template by {{.EngineConfig.CloudInitTemplate}}

access the hcloud config structure {{.HcloudConfig}}
For example accessing the api token by {{.HcloudConfig.ApiToken}}

access the aws config structure {{.AwsConfig}}
For example accessing the role arn  by {{.AwsConfig.RoleArn}}
For example accessing the access key {{ .AwsConfig.AccessKeyID }}
For example accessing the secret key {{ .AwsConfig.SecretAccessKey }}
For example accessing the session token {{ .AwsConfig.SessionToken }}			

```
### go type structs accessible in template
if no special attribute type is declared the attribute type is string

#### EngineConfig
```
type EngineConfig 
    Kind                  
    CloudInitTemplate 
    SeedMachineName       
    Provider          
```

#### AwsConfig
```
type AwsConfig 
	Kind       
	KeyName    
	Region     
	ImageId    
	InstanceType
	RoleArn     
	SecurityGroupConfig
	IamProfile 
	Vpc        
	Tags       
	AccessKeyID
	SecretAccessKey
	SessionToken   


type SecurityGroupConfig 
	Name
	Description  
	InboundRules 

type InboundRules 
	IpRange 
	Port    

type VpcConfig 
	Create   
	VpcId    
	SubnetId 
	VpcCidr  
	PublicSubnetCidr
	PublicSubnet
  PrivateSubnetCidr
	PrivateSubnet
	CreateSsmEndpoints 
	CreateInternetGateway 
  CreateNatGateway

type NlbConfig
  Create
  Port
```

#### HcloudConfig
```
type HcloudConfig 
    Kind          
    ApiToken      
    SshKeys     []string     
    ServerType  
    Location     
    Image        
```
#### SeedVariables

```
type SeedVariables 
    Kind          
    Vars    map[interface{}]interface{}    
}

```

