package main

import (
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/suite"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

type HcloudAdapterTestSuite struct {
	suite.Suite
}

func TestHcloudAdapterTestSuite(t *testing.T) {
	suite.Run(t, new(HcloudAdapterTestSuite))
}

func (suite *HcloudAdapterTestSuite) TestDeploySeed() {
	engine, err := InitSeedEngine("./examples/seedfiles/seed_example.yaml", "./examples/credential_example.yaml")
	cloudinittemplate, err := engine.ProcessCloudInitTemplate()
	suite.NoError(err)
	seedRequest := &SeedRequest{
		UserData:         cloudinittemplate,
		Name:             engine.EngineConfig.SeedMachineName,
		ServerType:       engine.HcloudConfig.ServerType,
		Image:            engine.HcloudConfig.Image,
		StartAfterCreate: true,
		SshKeys:          engine.HcloudConfig.SshKeys,
		Labels:           engine.HcloudConfig.Labels,
		Location:         engine.HcloudConfig.Location,
	}

	seed, err := json.Marshal(seedRequest)
	suite.NoError(err)
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, request *http.Request) {
		w.WriteHeader(http.StatusOK)
		suite.Equal("POST", request.Method)
		suite.Equal(fmt.Sprintf("Bearer %s", engine.HcloudConfig.ApiToken), request.Header.Get("Authorization"))
		bodyBytes, err := ioutil.ReadAll(request.Body)
		suite.NoError(err)
		if err == nil {
			fmt.Println("body:", string(bodyBytes))
			suite.Equal(string(seed), string(bodyBytes))
		}
	}))
	defer ts.Close()

	hcloud, err := InitHcloudAdapter(engine.EngineConfig, engine.HcloudConfig)
	hcloud.ServerUrl = ts.URL
	suite.NoError(err)
	if suite.NotNil(hcloud) {
		err = hcloud.DeploySeed(cloudinittemplate)
		suite.NoError(err)
	}
}
