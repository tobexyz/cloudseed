package main

type dynMap = map[string]interface{}

type EngineConfig struct {
	Kind              string `yaml:"kind"`
	CloudInitTemplate string `yaml:"cloudinittemplate"`
	SeedMachineName   string `yaml:"seedmachinename"`
	Provider          string `yaml:"provider"`
}

type AwsConfig struct {
	Kind                string              `yaml:"kind"`
	KeyName             string              `yaml:"key-name"`
	Region              string              `yaml:"region"`
	ImageId             string              `yaml:"image-id"`
	InstanceType        string              `yaml:"instance-type"`
	RoleArn             string              `yaml:"role-arn"`
	SecurityGroupConfig SecurityGroupConfig `yaml:"security-group"`
	IamProfile          string              `yaml:"iam-profile"`
	Vpc                 VpcConfig           `yaml:"vpc"`
	Nlb                 NblConfig           `yaml:"nlb"`
	Tags                map[string]string   `yaml:"tags"`
	AccessKeyID         string
	SecretAccessKey     string
	SessionToken        string
}

type SecurityGroupConfig struct {
	Name         string         `yaml:"name"`
	Description  string         `yaml:"description"`
	InboundRules []InboundRules `yaml:"inboundrules"`
}
type InboundRules struct {
	IpRange string `yaml:"iprange"`
	Port    string `yaml:"port"`
}

type NblConfig struct {
	Create bool   `yaml:"create"`
	Port   string `yaml:"port"`
}

type VpcConfig struct {
	Create                bool   `yaml:"create"`
	VpcId                 string `yaml:"vpc-id"`
	SubnetId              string `yaml:"subnet-id"`
	VpcCidr               string `yaml:"vpc-cidr"`
	PublicSubnetCidr      string `yaml:"public-subnet-cidr"`
	PublicSubnet          bool   `yaml:"public-subnet"`
	PrivateSubnetCidr     string `yaml:"private-subnet-cidr"`
	PrivateSubnet         bool   `yaml:"private-subnet"`
	CreateSsmEndpoints    bool   `yaml:"create-ssm-endpoints"`
	CreateInternetGateway bool   `yaml:"create-igw"`
	CreateNatGateway      bool   `yaml:"create-natgw"`
}

type HcloudConfig struct {
	Kind       string            `yaml:"kind"`
	ApiToken   string            `yaml:"apitoken"`
	SshKeys    []string          `yaml:"ssh-keys"`
	ServerType string            `yaml:"server-type"`
	Location   string            `yaml:"location"`
	Image      string            `yaml:"image"`
	Labels     map[string]string `yaml:"labels"`
}

type SeedVariables struct {
	Kind string `yaml:"kind"`
	Vars dynMap `yaml:"vars"`
}
