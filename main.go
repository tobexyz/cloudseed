package main

import (
	"flag"
	"fmt"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	fmt.Println("Usage: cloudseed -sensitiveSeedYamlFile=path_to_yamlfile -publicSeedYamlFile=path_to_yamlfile -dry-run\n Defaults: sensitiveSeedYamlFile=./credentials.yaml")

	publicSeedYamlfile := flag.String("publicSeedYamlFile", "missing", "a string")
	sensitiveSeedYamlFile := flag.String("sensitiveSeedYamlFile", "./credentials.yaml", "a string")
	dryRun := flag.Bool("dry-run", false, "dry run flag")
	flag.Parse()

	fmt.Printf("sensitiveSeedYamlFile: '%s'\n", *sensitiveSeedYamlFile)
	if *sensitiveSeedYamlFile == "missing" || *sensitiveSeedYamlFile == "" {
		fmt.Println("parameter sensitiveSeedYamlFile set")
		os.Exit(1)
	}

	fmt.Printf("publicSeedYamlfile: '%s'\n", *publicSeedYamlfile)
	if *publicSeedYamlfile == "missing" || *publicSeedYamlfile == "" {
		fmt.Println("parameter publicSeedYamlfilenot set")
		os.Exit(1)
	}
	//init
	var hcloud *HcloudAdapter
	var aws *AwsAdapter
	engine, err := InitSeedEngine(*publicSeedYamlfile, *sensitiveSeedYamlFile)
	check(err)
	if !*dryRun {
		if engine.EngineConfig.Provider == "hcloud" {
			fmt.Println("starting seed using hcloud")
			hcloud, err = InitHcloudAdapter(engine.EngineConfig, engine.HcloudConfig)
			check(err)
		} else if engine.EngineConfig.Provider == "aws" {
			fmt.Println("starting seed using aws")
			aws, err = InitAwsAdapter(&engine.EngineConfig, &engine.AwsConfig)
			check(err)
			cred, err := aws.Credentials.Get()
			check(err)
			engine.AwsConfig.AccessKeyID = cred.AccessKeyID
			engine.AwsConfig.SecretAccessKey = cred.SecretAccessKey
			engine.AwsConfig.SessionToken = cred.SessionToken
		} else {
			fmt.Printf("unknow cloud provider: %s\n", engine.EngineConfig.Provider)
		}
	}

	cloudinitString, err := engine.ProcessCloudInitTemplate()
	check(err)
	//deploy
	if *dryRun {
		fmt.Println(cloudinitString)
	} else {
		if engine.EngineConfig.Provider == "hcloud" {
			fmt.Println("starting seed using hcloud")
			err = hcloud.DeploySeed(cloudinitString)
			check(err)
		} else if engine.EngineConfig.Provider == "aws" {
			fmt.Println("starting seed using aws")
			err = aws.DeploySeed(cloudinitString)
			check(err)
		} else {
			fmt.Printf("unknow cloud provider: %s\n", engine.EngineConfig.Provider)
		}
	}
}
